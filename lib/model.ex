defmodule Model.Notification do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Poison.Encoder, only: []}
  schema "notifications" do
      field :title,       :string
      field :message,     :string
      field :finished,    Ecto.DateTime
      field :pinned,      :boolean, default: false
      field :archived,    :boolean, default: false
      timestamps
  end

  def changeset(row, params \\ :empty) do
    row
    |> cast(params, ~w(id), ~w(title message finished pinned archived))
  end
end


defimpl Poison.Encoder, for: Model.Notification do
  def encode(row, options) do
    entity = row
      |> Map.take(~w(id title message pinned archived)a)
      |> Map.put(:duration, row.finished |> _duration)
      |> Map.put(:created, row.inserted_at |> _format_datetime)
    Poison.encode!(entity, options)
  end

  defp _format_datetime(time) do
    time |> Ecto.DateTime.to_erl |> Timex.DateTime.from |> Timex.format!("{ISO:Extended}")
  end

  defp _duration(finished) do
    to    = finished |> Ecto.DateTime.to_erl
    from  = Ecto.DateTime.utc |> Ecto.DateTime.to_erl
    {days, _} = :calendar.time_difference(from, to)
    days
  end
end