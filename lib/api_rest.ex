# 接口注册
defmodule Api.Rest do
  use Maru.Router

  namespace :notifications do
    mount Rest.Notification
  end

  rescue_from Ecto.NoResultsError do
    conn |> put_status(404) |> json(%{reason: :not_found})
  end

  rescue_from Maru.Exceptions.InvalidFormatter do
    conn |> put_status(500) |> json(%{reason: :invalid_param})
  end

  rescue_from Maru.Exceptions.Validation do
    conn |> put_status(500) |> json(%{reason: :invalid_param})
  end

  rescue_from :all do
    conn |> put_status(500) |> json(%{error: :server_error})
  end
end
