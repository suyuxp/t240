defmodule Api do
	use Application

	def start(_type, _args) do
		{:ok, sup} = Api.Supervisor.start_link
		{:ok, sup}
	end
end
