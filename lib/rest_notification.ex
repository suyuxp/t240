# ================================================================
# Restful APIs
# ================================================================

defmodule Rest.Notification do
  use Maru.Router
  import Ecto.Query
  alias Api.Repo
  alias Model.Notification

  # --------------------------------------------------------------
  # GET /?archived=false
  # --------------------------------------------------------------
  params do
    requires :archived, type: Boolean, default: false
  end
  get do
    query = from(a in Notification,
              where: a.archived == ^params[:archived],
              order_by: [desc: a.pinned, desc: a.inserted_at])
    conn |> json(query |> Repo.all)
  end


  # --------------------------------------------------------------
  # POST /
  # --------------------------------------------------------------
  params do
    requires :title,    type: String
    requires :message,  type: String
    requires :duration, type: Integer, default: 999999
    requires :pinned,   type: Boolean, default: false
  end
  post do
    %{title: title, message: message, duration: duration, pinned: pinned} = params
    row = %{
            title:    title,
            message:  message,
            finished: duration |> _finished_at,
            pinned:   pinned,
            archived: false
          }
    conn |> json(Notification |> struct(row) |> Repo.insert!)
  end


  # --------------------------------------------------------------
  # PUT /:id
  # --------------------------------------------------------------
  params do
    requires :id, type: Integer
    optional :archived, type: Boolean, values: [false, true]
    optional :pinned,   type: Boolean, values: [false, true]
    optional :duration, type: Integer
    optional :title,    type: String
    optional :message,  type: String
  end
  put "/:id" do
    row = Notification |> Repo.get!(params[:id])
    changes = %{
      id:         params[:id],
      title:      params[:title] || row.title,
      message:    params[:message] || row.message,
      finished:   _finished_at(params[:duration], row.finished),
      pinned:     _boolParam(params[:pinned], row.pinned),
      archived:   _boolParam(params[:archived], row.archived)
    }
    updated = row |> Notification.changeset(changes) |> Repo.update!
    conn |> json(updated)
  end

  defp _boolParam(nil, oldValue), do: oldValue
  defp _boolParam(newValue, _),   do: newValue

  # --------------------------------------------------------------
  # DELETE /:id
  # --------------------------------------------------------------
  params do
    requires :id, type: Integer
  end
  delete "/:id" do
    row = Notification |> Repo.get!(params[:id])
    row |> Repo.delete!
    conn |> json(row)
  end


  defp _finished_at(nil, finished), do: finished
  defp _finished_at(duration, _), do: _finished_at(duration)
  defp _finished_at(duration) do
    date = :calendar.date_to_gregorian_days(elem(:calendar.local_time, 0)) + duration
    Ecto.DateTime.from_erl({:calendar.gregorian_days_to_date(date), {23,59,59}})
  end
end
