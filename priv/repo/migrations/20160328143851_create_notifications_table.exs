defmodule Api.Repo.Migrations.CreateNotificationsTable do
  use Ecto.Migration

  def up do
    create table(:notifications) do
      add :title,       :string
      add :message,     :string
      add :finished,    :datetime
      add :pinned,      :boolean, default: false
      add :archived,    :boolean, default: false
      timestamps
    end
  end

  def down do
    drop table(:notifications)
  end
end
