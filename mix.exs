defmodule Api.Mixfile do
  use Mix.Project

  def project do
    [app: :api,
     version: "0.0.1",
     elixir: "~> 1.2",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps(Mix.env)]
  end

  # Configuration for the OTP application
  #
  # Type "mix help compile.app" for more information
  def application do
    [applications: apps(Mix.env),
     mod: {Api, []}]
  end

  # Dependencies can be Hex packages:
  #
  #   {:mydep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:mydep, git: "https://github.com/elixir-lang/mydep.git", tag: "0.1.0"}
  #
  # Type "mix help deps" for more examples and options
  defp apps(:prod) do
    [:timex, :logger, :maru, :postgrex, :ecto]
  end

  defp apps(_) do
    [:exsync | apps(:prod)]
  end


  defp deps(:prod) do
    [
      {:maru, "~> 0.9.5"},
      {:poison, "~> 2.0"},
      {:timex, "~> 2.1.1"},
      {:postgrex, ">= 0.0.0"},
      {:ecto, "~> 2.0.0-beta"}
    ]
  end

  defp deps(_) do
    [{:exsync, "~> 0.1", only: :dev} | deps(:prod)]
  end
end
