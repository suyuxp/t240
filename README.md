### 开发

下载安装 Elixir 开发环境后，克隆代码
```
git clone https://bitbucket.org/suyuxp/T240.git
```

然后修改 config/config.exs 中的数据库配置
```
config :api, Api.Repo,
  adapter: Ecto.Adapters.Postgres,
  database: "cloapdb",
  username: "docker",
  password: "password",
  hostname: "db"
```

初始化数据库
```
mix ecto.create
mix ecto.migrate
```

运行以下命令，即可访问 http://localhost:8880
```
iex -S mix
```


### 部署

采用 docker 部署的方法
```
docker build -t index.alauda.cn/gugud/t240 -f docker/Dockerfile .
docker-compose -f docker/docker-compose.yml up -d
```

按 docker-compose.yml 中的配置，可访问 http://localhost:3000

环境变量:

1. PG_USER, 数据库用户账号
1. PG_PASSWD, 数据库用户口令
1. PG_DB, 数据库名称
1. PG_HOST, 数据库主机
